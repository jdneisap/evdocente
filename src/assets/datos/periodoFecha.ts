export interface PeriodoFecha {
    periodo: string;
    fecha: string;
}

export const PERIODOS: PeriodoFecha =
{
    //Mantener el formato indicado en periodo y fecha
    //Formato para periodo año-1 , año-2, año-3
    periodo: '2021-3',
    //Formato para fecha hora dia mes y año
    fecha: '12:00 del día 18 de Marzo de 2022'
};

export const textoEvaluacion = [
  "Cumplió con las actividades acordadas, para las horas lectivas y no lectivas establecidas en el plan de trabajo.",
  "Participo en la planeación y ejecución de las actividades académicas, gremiales o de integración del Proyecto Curricular, la Facultad y la Universidad.",
  "Durante el último año, los proyectos de grado que dirigió, revisó y evaluo han merecido reconocimiento académico comprobable como el caso de meritorio o laureado.",
  "Demostró actitudes de cumplimiento, respeto, empatía, colaboración y participación activa, frente al Proyecto Curricular, la Facultad y la Universidad.",
  "Participo en la conformación de grupos o semilleros de investigación.",
  "Realizó funciones de docencia, extensión, investigación y creación.",
  "Durante el último año, ha participado en seminarios de actualización,conferencias, cursos de capacitación o estudios de educación continuada.",
  "Los artículos, libros, ponencias o muestras artísticas que realizó en el último año, fueron publicados en revistas indexadas o presentados en eventos de amplio reconocimiento a nivel nacional o internacional.",
  "Generó actividades innovadoras que son determinantes para la Universidad, mediante su participación en los comités, consejos y grupos de trabajo a los que pertenece.",
  "Cumplió a cabalidad con la entrega de programas, informes y evaluaciones para el Proyecto Curricular y la Decanatura, con relación al plan de trabajo que presentó al inicio del curso.",
  "Cumplio con el registro de notas según los cortes programados por la Universidad."
]
