import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempoCompletoComponent } from './tiempo-completo.component';

describe('TiempoCompletoComponent', () => {
  let component: TiempoCompletoComponent;
  let fixture: ComponentFixture<TiempoCompletoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TiempoCompletoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempoCompletoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
