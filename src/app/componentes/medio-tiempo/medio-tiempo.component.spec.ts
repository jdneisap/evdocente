import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedioTiempoComponent } from './medio-tiempo.component';

describe('MedioTiempoComponent', () => {
  let component: MedioTiempoComponent;
  let fixture: ComponentFixture<MedioTiempoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedioTiempoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedioTiempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
