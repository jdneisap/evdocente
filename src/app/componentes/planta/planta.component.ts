import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Decision, DECISIONES } from 'src/app/modelos/decision';
import { Nota, NOTAS } from 'src/app/modelos/nota';
import { PeriodoFecha, PERIODOS, textoEvaluacion} from 'src/assets/datos/periodoFecha';

@Component({
  selector: 'app-planta',
  templateUrl: './planta.component.html',
  styleUrls: ['./planta.component.scss']
})
export class PlantaComponent implements OnInit {

  selectedNota = ["","","","","","","","","","",""];
  arrayPrueba = textoEvaluacion
  ContinSemestre : any;

  nomProfesor: string;
  cedProfesor: number;
  asigProfesor: string;
  codAsignatura: number;
  gruAsignatura: number;

  notas: Nota[] = NOTAS;
  decisiones: Decision[] = DECISIONES;
  periodo: PeriodoFecha = PERIODOS;

  formularioPlantaForm: FormGroup;

  @ViewChild('fplantaform') formularioPlantaFormDirective;

  formErrors = {
    'nombreProfesor': '',
    'cedulaProfesor': '',
    'asignaturaProfesor': '',
    'codigoAsignatura': '',
    'grupoAsignatura': ''
  };

  validationMessages = {
    'nombreProfesor': {
      'required': 'Nombre del profesor es requerido',
      'minlength': 'El nombre del profesor debe ser de al menos 6 caracteres',
      'maxlength': 'El nombre del profesor no debe ser mayor de 35 caracteres'
    },
    'cedulaProfesor': {
      'required': 'Número de identificación del profesor es requerido',
      'minlength': 'El número de identificación debe ser de al menos 6 números',
      'maxlength': 'El número de identificación no debe ser mayor de 14 números',
      'pattern': 'Este campo solo puede contener números'
    },
    'asignaturaProfesor': {
      'required': 'Nombre de asignatura es requerido',
      'minlength': 'El nombre de la asignatura debe ser de al menos 6 caracteres',
      'maxlength': 'El nombre de la asignatura no debe ser mayor de 35 caracteres'
    },
    'codigoAsignatura': {
      'required': 'Código de la asignatura es requerido',
      'minlength': 'El código de la asignatura debe ser de al menos 1 número',
      'maxlength': 'El código de la asignatura no debe ser mayor de 8 números',
      'pattern': 'Este campo solo puede contener números'
    },
    'grupoAsignatura': {
      'required': 'El grupo de la asignatura es requerido',
      'minlength': 'El grupo de la asignatura debe ser de al menos 1 números',
      'maxlength': 'El grupo de la asignatura no debe ser mayor de 5 números',
      'pattern': 'Este campo solo puede contener números'
    }
  }

  constructor(private cb: FormBuilder) { }

  ngOnInit(): void {
    this.crearForm();
  }

  changeNota(event,i){
    this.selectedNota[i] = event
  }

  changeContinSemestre(event){
    this.ContinSemestre = event
    console.log(event)
  }

  crearForm() {
    this.formularioPlantaForm = this.cb.group({
      nombreProfesor: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(35)]],
      cedulaProfesor: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14), Validators.pattern]],
      asignaturaProfesor: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(35)]]   ,
      codigoAsignatura: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(8), Validators.pattern]],
      grupoAsignatura: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(5), Validators.pattern]]
    });
    this.formularioPlantaForm.valueChanges.subscribe(data => this.onValueChanged(data));
  }

  onValueChanged(data?): any {
    if (!this.formularioPlantaForm) {
      return;
    }
    const form = this.formularioPlantaForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  imprimir(){
    window.print();
  }

}
