import { Routes } from '@angular/router';
import { InicioComponent } from "../componentes/inicio/inicio.component";
import { CatedraComponent } from "../componentes/catedra/catedra.component";
import { MedioTiempoComponent } from "../componentes/medio-tiempo/medio-tiempo.component";
import { TiempoCompletoComponent } from "../componentes/tiempo-completo/tiempo-completo.component";
import { PlantaComponent } from "../componentes/planta/planta.component";

export const routes: Routes = [
    {path: '', component: InicioComponent},
    {path: 'catedra', component: CatedraComponent},
    {path: 'medioTiempo', component: MedioTiempoComponent},
    {path: 'tiempoCompleto', component: TiempoCompletoComponent},
    {path: 'planta', component: PlantaComponent},
    {path: '', redirectTo: '/', pathMatch: 'full'},
    { path: '**', redirectTo: '/'}
];
