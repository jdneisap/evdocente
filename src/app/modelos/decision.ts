export interface Decision {
    nombre: string;
}

export const DECISIONES: Decision[] = [
    {
        nombre: 'Si',
    },
    {
        nombre: 'No',
    }
];