export interface Nota {
    nombre: string;
}

export const NOTAS: Nota[] = [
    {
        nombre: '5',
    },
    {
        nombre: '4',
    },
    {
        nombre: '3',
    },
    {
        nombre: '2',
    },
    {
        nombre: '1',
    },
    {
        nombre: 'N.A',
    }
];